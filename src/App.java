public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");

        System.out.println(App.sumNumbersV1());
        App app = new App();
        int sum = app.sumNumbersV11();
        System.out.println(sum);
        //
        int[]number = {1, 5, 10} ;
        int sum2 = app.sumNumbersV2(number);
        System.out.println("Tông số nguyên của măng là: " + sum2);
        //
        App.printHello(24);
        printHello(99);
    }

    public static int sumNumbersV1() {
        int sum = 0;
        for(int i = 0; i <= 100; i ++ ){
            sum = sum + i;
        };
        return sum;
    }
    public int sumNumbersV11() {
        int sum = 0;
        for(int i = 0; i <= 100; i ++ ){
            sum = sum + i;
        };
        return sum;
    }
    // tính tổng só nguyên
    public int sumNumbersV2(int [] number) {
        int sum = 0;
        for(int i = 0; i < number.length; i ++ ){
            sum = sum + number[i];
        };
        return sum;
    }
    // hiển thị thông báo só truyền vào 
    public static void printHello(int number) {
        if(number <=0){
            System.out.println("Đây là số nhỏ hơn hoặc bằng 0");
        } else {
            if (number % 2 == 0) {
                System.out.println("Đây là số chẵn!");
            } else {
                System.out.println("Đây là số lẻ!");
            }

        }
    }
}
